const data  = {

    items: [
        {
            desc: 'Google',
            icon: '/img/logo/google.svg',
            link: 'https://www.google.com/'
        },
        {
            desc: '谷歌云',
            icon: '/img/logo/supercloud.svg',
            link: 'https://console.cloud.google.com/'
        },
        {
            desc: 'YouTube',
            icon: '/img/logo/youtube.svg',
            link: 'https://www.youtube.com/'
        },
        {
            desc: 'ChatGPT',
            icon: '/img/logo/openai.svg',
            link: "https://chatgpt.com/?model=auto",
            style: 'background-color: #fff;'
        },
        {
            desc: '百度搜索引擎服务',
            icon: 'https://files.codelife.cc/website/baidu.svg',
            link: 'https://www.baidu.com/'
        },
        {
            desc: '谷歌新闻',
            icon: '/img/logo/google_news.webp',
            link: 'https://news.google.com/topstories'
        },
        {
            desc: 'IT之家，百度指数排名第一的前沿科技门户网站，青岛软媒旗下。',
            icon: '/img/logo/ithome.svg',
            link: 'https://www.ithome.com/'
        },
        {
            desc: 'Maven – Welcome to Apache Maven',
            icon: '/img/logo/apache-maven-icon.png',
            link: 'https://mvnrepository.com/'
        },
        {
            desc: 'GitHub 科技爱好者周刊',
            icon: '/img/logo/ruanyf.jpeg',
            link: 'https://github.com/ruanyf/weekly'
        },
        {
            desc: '阿里云',
            icon: '/img/logo/aliyun.svg',
            link: 'https://home.console.aliyun.com/',
            style: 'background-color: #fd7717;'
        },
        {
            desc: '正则表达式',
            icon: '/img/logo/regex.png',
            link: 'https://regex101.com/'
        },
        {
            desc: '程序员工具箱',
            icon: '/img/logo/tools.svg',
            link: 'https://tool.lu/mine/'
        },
        {
            desc: 'CyberChef 开源Web工具',
            icon: '/html/cyber-chef/images/cyberchef-128x128.png',
            link: "/html/cyber-chef/#recipe=JSON_Beautify('%20%20%20%20',false)&input=eyAgICAiaXAxIjogIjIyMi4xODcuMTE0LjQ5IiwgICAgImlwMiI6ICIxNDAuMjA2LjE4NS4yMDIifQ"
        },
        {
            desc: '原版软件 NEXT, ITELLYOU',
            icon: '/img/logo/itellyou.png',
            link: "https://next.itellyou.cn/Original/Index"
        },
        {
            desc: 'iconfont 阿里妈妈MUX倾力打造的矢量图标管理、交流平台',
            icon: '/img/logo/iconfont.svg',
            link: "https://www.iconfont.cn/collections/index"
        },
        {
            desc: '站长工具 - 站长之家',
            icon: '/img/logo/chinaz.png',
            link: "https://tool.chinaz.com/"
        },
        {
            desc: '阿里巴巴开源镜像站，免费提供Linux镜像下载服务',
            icon: 'https://gw.alicdn.com/imgextra/i1/O1CN01jmU7851wNA2cxuYUW_!!6000000006295-1-tps-284-220.gif',
            link: "https://developer.aliyun.com/mirror/",
            style: 'background-color: #fd7717;'
        },
        {
            desc: '花生壳管理',
            icon: '/img/logo/oray.png',
            link: "http://sunlogin.oray.com/console/remote"
        },
        {
            desc: 'Pi状态监控',
            icon: '/img/logo/adafruit.png',
            link: "https://io.adafruit.com/AdamYao/dashboards/raspberrypi",
            style: "background-color: black"
        },
        {
            desc: 'Bootstrap是Twitter推出的一个用于前端开发的开源工具包',
            icon: '/img/logo/bootstrap.svg',
            link: "https://www.bootcdn.cn/"
        },
        {
            desc: 'Homebrew',
            icon: '/img/logo/homebrew.svg',
            link: "https://brew.sh/zh-cn/"
        },
        {
            desc: 'Choco',
            icon: '/img/logo/chocolatey.svg',
            link: "https://community.chocolatey.org/packages"
        },
        {
            desc: 'Winget',
            icon: '/img/logo/winstall.svg',
            link: "https://winstall.app/"
        }
    ]

}

